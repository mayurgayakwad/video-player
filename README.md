# Video-player
Welcome to the Video Player project! This React.js application allows users to play videos from a playlist with essential features such as play/pause toggle, seeking, timer, autoplay, and speed selector. Additionally, users can reorder the playlist to customize their viewing experience.


## Getting started

To get started with the project, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Install dependencies using npm install.
4. Run the application locally using npm start.

## Features
* Play/Pause toggle
* Seeking functionality
* Timer displaying current playback time and duration 
* Autoplay
* Speed selector for playback speed adjustment
* Video Search in Playlist
* Reorder videos in the playlist.
* Autoplay next video in the playlist
* Current Video Playing Highlight

## Project Deployment & Video
- **[Video Player App](https://videoplayerproject.vercel.app/)**: Visit this link to access the deployed video player application.
- **[Watch a Demonstration Video](https://drive.google.com/file/d/1CYj9DmJ_ghGQxYrLUsKuJgPM7jQWvD24/view)**: Click here to watch a video demonstrating how the video player app works.

## Usage
1. Open the application in your web browser.
2. Browse the playlist to find the desired video.
3. Click on a video to load and play it. You will see the video player and playlist.
4. You can change the track/video from the playlist.
5. Use the controls provided to adjust playback settings as needed.


## Authors
[Mayur Gayakwad](https://www.linkedin.com/in/mayurgayakwad/)