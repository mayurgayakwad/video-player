import React, { useState } from "react";
import "./PlaylistStyle.css";
import { FcSearch } from "react-icons/fc";
import SortablePlaylist from "./SortablePlaylist";
import { FaPlayCircle } from "react-icons/fa";

const Playlist = ({
  videosData,
  setVideoUrl,
  setSelectedVideo,
  isSortablePlaylist = false,
  selectedVideo,
  playNextVideo,
  setPlayNextVideo,
}) => {
  const { videos } = videosData.categories[0];
  const [search, setSearch] = useState("");

  const handleVideoPlay = (video) => {
    setVideoUrl(video.sources[0]);
    setSelectedVideo(video);
  };

  const VideoItem = ({ video }) => {
    return (
      <div className="video-item" onClick={() => handleVideoPlay(video)}>
        <img className="thumbnail" src={video.thumb} alt={video.title} />
        <div className="play-button">
          <FaPlayCircle style={{ width: 50, height: 50 }} />
        </div>
        <div className="video-details">
          <h2 className="video-title">
            {video.title} | {video.subtitle}
          </h2>
          <p className="video-description">{video.description}</p>
        </div>
      </div>
    );
  };

  return (
    <div className="playlist">
      <div className="container-input">
        <input
          type="text"
          placeholder="Search"
          name="text"
          className="input"
          onChange={(event) => setSearch(event.target.value.toLowerCase())}
        />
        <FcSearch className="search-icon" />
      </div>
      <div className={`${ isSortablePlaylist ? "reorder-video-list" : "video-list"}`}>
        {isSortablePlaylist && (
          <SortablePlaylist
            video={videos}
            setVideoUrl={setVideoUrl}
            setSelectedVideo={setSelectedVideo}
            searchString={search}
            selectedVideo={selectedVideo}
            playNextVideo={playNextVideo}
            setPlayNextVideo={setPlayNextVideo}
          />
        )}

        {!isSortablePlaylist &&
          videos.filter((vid) => search ? vid.title.toLowerCase().includes(search) : true)
            .map((video, index) => <VideoItem key={index} video={video} />)}
        {(videos.filter((vid) => search ? vid.title.toLowerCase().includes(search) : true).length === 0) && (<h2>Sorry, no matches found.</h2>)}
      </div>
    </div>
  );
};

export default Playlist;
