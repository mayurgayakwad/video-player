import React, { useState } from "react";
import VideoPlayer from "./VideoPlayer";
import Playlist from "./Playlist";
import "./VideoPlayerWithPlaylistStyle.css";

function VideoPlayerWithPlaylist({
  videoUrl,
  videosData,
  setVideoUrl,
  selectedVideo,
  setSelectedVideo,
}) {
  const [playNextVideo, setPlayNextVideo] = useState(false);

  return (
    <div className="video-player-with-playlist">
      <div className="video-player-container">
        <div className="video-player">
          <VideoPlayer videoUrl={videoUrl} selectedVideo={selectedVideo} setPlayNextVideo={setPlayNextVideo} />
        </div>
      </div>
      <div className="playlist-container">
        <div className="playlist-section">
          <Playlist
            videosData={videosData}
            setVideoUrl={setVideoUrl}
            setSelectedVideo={setSelectedVideo}
            setPlayNextVideo={setPlayNextVideo}
            isSortablePlaylist
            playNextVideo={playNextVideo}
            selectedVideo={selectedVideo}
          />
        </div>
      </div>
    </div>
  );
}

export default VideoPlayerWithPlaylist;
