import React, { useEffect, useRef, useState } from "react";
import "./PlaylistStyle.css";

function SortablePlaylist({
  video,
  setVideoUrl,
  setSelectedVideo,
  searchString,
  selectedVideo,
  setPlayNextVideo,
  playNextVideo,
}) {
  const [sortableVideos, setSortableVideos] = useState(video);
  const [dragVideoIndex, setDragVideoIndex] = useState(null);
  const [dragOverVideoIndex, setDragOverVideoIndex] = useState(null);
  const ref = useRef(null);
  const { title } = selectedVideo;

  const handleVideoPlay = (video) => {
    setVideoUrl(video.sources[0]);
    setSelectedVideo(video);
  };

  useEffect(() => {
    if (playNextVideo) {
      const currentVideoIndex = ref.current;
      const nextIndex = currentVideoIndex < sortableVideos.length - 1 ? currentVideoIndex + 1 : 0;
      setVideoUrl(sortableVideos[nextIndex].sources[0]);
      setSelectedVideo(sortableVideos[nextIndex]);
      setPlayNextVideo(false);
    }
  }, [playNextVideo]);

  const handleDragStart = (index) => {
    setDragVideoIndex(index);
  };

  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const handleDrop = (index) => {
    if (dragVideoIndex === null || dragOverVideoIndex === null) return;

    const updatedVideos = [...sortableVideos];
    const draggedVideo = updatedVideos.splice(dragVideoIndex, 1)[0];
    updatedVideos.splice(index, 0, draggedVideo);

    setSortableVideos(updatedVideos);
    setDragVideoIndex(null);
    setDragOverVideoIndex(null);
  };

  const handleDragEnter = (index) => {
    setDragOverVideoIndex(index);
  };

  const handleDragLeave = () => {
    setDragOverVideoIndex(null);
  };

  const handleDragEnd = () => {
    setDragVideoIndex(null);
    setDragOverVideoIndex(null);
  };

  const handleClassName = (video, index) => {
    const reorderVideoItemClass = "reorder-video-item";
    const dragOverClass = dragOverVideoIndex === index ? "drag-over" : "";
    const currentVideoPlayingClass = title === video.title ? "current-video-playing" : "";

    if (title === video.title) {
      ref.current = index;
    }

    const combinedClassName = `${reorderVideoItemClass} ${dragOverClass} ${currentVideoPlayingClass}`;

    return combinedClassName;
  };

  return (
    <>
      {sortableVideos.filter((vid) => searchString ? vid.title.toLowerCase().includes(searchString) : true)
        .map((video, index) => (
          <li
            key={index}
            draggable
            className={handleClassName(video, index)}
            onClick={() => handleVideoPlay(video)}
            onDragStart={() => handleDragStart(index)}
            onDragOver={handleDragOver}
            onDrop={() => handleDrop(index)}
            onDragEnter={() => handleDragEnter(index)}
            onDragLeave={handleDragLeave}
            onDragEnd={handleDragEnd}
          >
            <img
              className="reorder-thumb-img"
              src={video.thumb}
              alt={video.title}
            />
            <div className="reorder-video-details">
              <div className="reorder-video-title">
                <p>{video.title}</p>
                <p>{video.subtitle}</p>
              </div>
              <p className="reorder-video-description">{video.description}</p>
            </div>
          </li>
        ))}
    </>
  );
}

export default SortablePlaylist;
