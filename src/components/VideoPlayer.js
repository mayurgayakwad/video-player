import React from "react";

const VideoPlayer = ({ videoUrl, selectedVideo: video, setPlayNextVideo }) => {
  const playNextVideo = () => {
    setPlayNextVideo(true)
    console.log('Video Ended')
  }
  return (
    <>
      <div style={{ position: "relative" }}>
        <video
          src={videoUrl}
          onEnded={playNextVideo}
          autoPlay
          controls
          style={{ width: "100%", borderRadius: "12px" }}
        />
      </div>
      <div className="video-item" style={{ marginTop: "25px" }}>
        <div className="">
          <h2 className="" style={{ display: "inline" }}>
            {video.title} | {video.subtitle}
          </h2>
          <p style={{ width: "100%", lineHeight:1.5, fontWeight:300 }}>Description : {video.description}</p>
        </div>
      </div>
    </>
  );
};

export default VideoPlayer;
