import "./App.css";
import videos from "./videosData";
import Playlist from "./components/Playlist";
import { useState } from "react";
import VideoPlayer from "./components/VideoPlayer";
import VideoPlayerWithPaylist from "./components/VideoPlayerWithPaylist";
import { IoArrowBack } from "react-icons/io5";

function App() {
  const [videoUrl, setVideoUrl] = useState(null);
  const [selectedVideo, setSelectedVideo] = useState(null);
  console.log({ videoUrl });
  return (
    <div className="App">
      <div style={{ display: 'flex', alignItems: 'center', marginBottom:25 }}>
        {videoUrl && <IoArrowBack title="Go Back" style={{ width: 50, height: 50, cursor: 'pointer' }} onClick={() => window.location.reload()} />}
        <h1 className="video-player-heading" style={{ margin: '0 auto' }}>Video Player App</h1>
      </div>

      {!videoUrl && <Playlist videosData={videos} setVideoUrl={setVideoUrl} setSelectedVideo={setSelectedVideo} />}
      {videoUrl && <VideoPlayerWithPaylist videoUrl={videoUrl} videosData={videos} setVideoUrl={setVideoUrl} selectedVideo={selectedVideo} setSelectedVideo={setSelectedVideo} />}
    </div>
  );
}

export default App;
